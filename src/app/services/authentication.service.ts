
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from "../../environments/environment";

export interface IResponse{
    status: number;
    message: string;
}
 
@Injectable()
export class AuthenticationService {
    private loginUrl = `${environment.dataUrl}`;

    constructor(private http: HttpClient) { }
    
 
    login(): Observable<IResponse> {
        return this.http.get<IResponse>(this.loginUrl);
    }
 
}