import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './login.component';
import { LoginRoutingModule } from './login-routing.module';
//import { LoginLibModule } from '../../../../projects/ngx-lib/src/lib/login/login-lib.module';
import { LoginLibModule } from '@corsaronero/ngx-lib';
import { AuthenticationService } from '../../services/authentication.service';

@NgModule({
  declarations: [LoginComponent],
  imports: [CommonModule, LoginRoutingModule, LoginLibModule],
  providers:[AuthenticationService],
  exports: [LoginComponent]
})
export class LoginModule {}