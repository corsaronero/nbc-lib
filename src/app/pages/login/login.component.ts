import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../../services/authentication.service';
import { SubSink } from 'subsink';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  errMsg: string;
  subSink = new SubSink();

  constructor(private authenticationService: AuthenticationService) { }

  ngOnInit(): void {
  }

  login(ev){
    this.subSink.add(this.authenticationService.login()
      .subscribe(res => {
          console.log("response", res);
          if(res[0].status == 200){
            console.log("Happy Login");
          }
          else{
            this.errMsg = res[0].message; 
          }
        },
        error => {
          console.log("data", <any>error)
          this.errMsg = <any>error.error.message; 
        },
        () => console.log("") // run this code in all cases
      )
    )
  }

}
