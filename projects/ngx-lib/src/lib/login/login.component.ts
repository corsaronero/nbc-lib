import { Component, Input, OnInit, EventEmitter, Output } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';

@Component({
  selector: 'nbc-login',
  templateUrl: './login.component.html',
})
export class LoginComponent implements OnInit {

  email = new FormControl();
  password = new FormControl();
  signupForm: FormGroup;
  
  @Input() errMsg: string;
  @Output() submit = new EventEmitter<string>();

  constructor(private formBuilder: FormBuilder) { }

  ngOnInit() {
      this.email = this.formBuilder.control(null, [
        Validators.required,
        Validators.minLength(4),
        Validators.maxLength(40),
        Validators.pattern(/[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/)
      ]);
      this.password = this.formBuilder.control(null, [ Validators.minLength(6) ]);

      this.signupForm = this.formBuilder.group({
        email: this.email,
        password: this.password
      });
  }

  formSubmit(ev){
    ev.stopPropagation();
    ev.preventDefault();
    this.submit.emit(this.signupForm.value);
  }
}
